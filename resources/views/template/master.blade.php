<!DOCTYPE html>
<html lang="zxx" dir="ltr">

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#24695C" />
    <!-- Site Properties -->
    <title>Investorich | Profitable Partnership</title>
    <!-- critical preload -->
    <link rel="preload" href="{{url('/')}}/equity/js/vendors/uikit.min.js" as="script">
    <link rel="preload" href="{{url('/')}}/equity/css/vendors/uikit.min.css" as="style">
    <link rel="preload" href="{{url('/')}}/equity/css/style.css" as="style">
    <!-- icon preload -->
    <link rel="preload" href="{{url('/')}}/equity/fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{url('/')}}/equity/fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <!-- font preload -->
    <link rel="preload" href="{{url('/')}}/equity/fonts/archivo-v9-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{url('/')}}/equity/fonts/archivo-v9-latin-300.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{url('/')}}/equity/fonts/archivo-v9-latin-700.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="{{url('/')}}/equity/img/icon.png" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">
    <!-- css -->
    <link rel="stylesheet" href="{{url('/')}}/equity/css/vendors/uikit.min.css">
    <link rel="stylesheet" href="{{url('/')}}/equity/css/style.css">
</head>

<body>
    <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <header>
        <!-- header content begin -->
        <div class="uk-section uk-padding-remove-vertical" id="home">
            <nav class="uk-navbar-container uk-navbar-transparent" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top;">
                <div class="uk-container" data-uk-navbar>
                    <div class="uk-navbar-left">
                        <div class="uk-navbar-item">
                            <!-- logo begin -->
                            <a class="uk-logo" href="{{route('homepage')}}">
                              <img src="https://dev-web.fib.partners/images/Investorich-alt.png" data-src="https://dev-web.fib.partners/images/Investorich-alt.png" alt="logo" width="175" data-uk-img>
                            </a>
                            <!-- logo end -->
                            <!-- navigation begin -->
                            <ul class="uk-navbar-nav uk-visible@m">
                              <li><a href="#home">Home</a></li>
                              <li><a href="#products">Products</a></li>
                              <li><a href="#benefits">Benefits</a></li>
                              <li><a href="#features">Features</a></li>
                            </ul>
                            <!-- navigation end -->
                        </div>
                    </div>
                    <div class="uk-navbar-right">
                        <div class="uk-navbar-item uk-visible@m in-optional-nav">
                            <a href="{{env('IB_LOGIN')}}" class="uk-button uk-button-text">Log in<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                            <a href="{{env('IB_REGISTER')}}" class="uk-button uk-button-primary uk-border-rounded text-white">Sign up<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <!-- header content end -->
    </header>
    <main>
        <!-- slideshow content begin -->
        <div class="uk-section uk-padding-remove-vertical in-slideshow-gradient">
            <div id="particles-js" class="uk-light in-slideshow uk-background-contain" data-uk-img data-uk-slideshow>
                <hr>
                <ul class="uk-slideshow-items">
                    <li class="uk-flex uk-flex-middle">
                        <div class="uk-container">
                            <div class="uk-grid-large uk-flex-middle" data-uk-grid>
                                <div class="uk-width-1-2@s in-slide-text">
                                    <p class="in-badge-text uk-text-small uk-margin-remove-bottom uk-visible@m"><span class="uk-label uk-label-danger in-label-small">News</span>Soft Launching Promotion. Register and Win the Prize (01-28 Feb 2022).</p>
                                    <h1 class="uk-heading-small">The world's first <span class="in-highlight">powerfull</span> Forex Business.</h1>
                                    <p class="uk-text-lead uk-visible@m">Get the most profitable schema for Introducing Brokerage — all within the same dashboard.</p>
                                    <div class="uk-grid-medium uk-child-width-1-3@m uk-child-width-1-2@s uk-margin-medium-top uk-visible@s" data-uk-grid data-market="TSLA,GOOGL,AAPL">
                                        <div>
                                            <div class="uk-card uk-card-small uk-card-secondary uk-card-body uk-border-rounded uk-flex uk-flex-middle">
                                                <div class="in-symbol-logo">
                                                    <i class="fas fa-arrow-circle-right uk-margin-small-left"></i>
                                                </div>
                                                <div class="in-price">
                                                    <h6 class="uk-margin-remove" style="padding:5px;"> COMMISSION<span class="uk-text-small"></span></h6>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="uk-card uk-card-small uk-card-secondary uk-card-body uk-border-rounded uk-flex uk-flex-middle">
                                                <div class="in-symbol-logo">
                                                    <i class="fas fa-arrow-circle-right uk-margin-small-left"></i>
                                                </div>
                                                <div class="in-price">
                                                    <h6 class="uk-margin-remove" style="padding:5px;"> POSITION<span class="uk-text-small"></span></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-visible@m">
                                            <div class="uk-card uk-card-small uk-card-secondary uk-card-body uk-border-rounded uk-flex uk-flex-middle">
                                                <div class="in-symbol-logo">
                                                    <i class="fas fa-arrow-circle-right uk-margin-small-left"></i>
                                                </div>
                                                <div class="in-price">
                                                    <h6 class="uk-margin-remove" style="padding:5px;"> SPREAD<span class="uk-text-small"></span></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="in-slide-img">
                                    <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-slide.png" alt="image-slide" width="652" height="746" data-uk-img>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>
                <div class="uk-container">
                    <div class="uk-position-relative" data-uk-grid>
                        <ul class="uk-slideshow-nav uk-dotnav uk-position-bottom-right uk-flex uk-flex-middle"></ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- slideshow content end -->

        <!-- section content begin -->
        <div class="uk-section in-equity-2 uk-background-contain uk-background-center" data-src="{{url('/')}}/equity/img/in-equity-2-bg.png" data-uk-img>
            <div class="uk-container uk-margin-top">
                <div class="uk-grid uk-flex uk-flex-center">
                    <div class="uk-width-2xlarge@m uk-text-center" id="benefits">
                        <span class="uk-label uk-label-warning">Profit on commission, position, and spread<i class="fas fa-arrow-right fa-xs uk-margin-small-left"></i></span>
                        <h1 class="uk-margin-top">Premium choice for your new experience in forex business</h1>
                        <p class="uk-text-lead uk-margin-medium-top">Harness the power of technology to make a quicker, smarter and more precise decision on manage business with updated margin in-out, lot calculation, P/L, floating and more</p>
                    </div>
                    <div class="uk-width-3-4@m uk-margin-medium-top uk-border-rounded">
                        <img class="uk-border-rounded" src="{{url('/')}}/equity/img/trading-statement-2.png" data-src="{{url('/')}}/equity/img/trading-statement-2.png" alt="image" height="600px" data-uk-img>
                    </div>
                    <div class="uk-width-2xlarge@m uk-margin-medium-top">
                        <div class="uk-grid uk-child-width-1-4@m uk-child-width-1-4@s uk-text-center in-feature-box" data-uk-grid>
                            <a href="#">
                                <span class="in-icon-wrap">
                                    <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-2-icon-4.svg" alt="icon-1" width="35" height="42" data-uk-img>
                                </span>
                                <p class="uk-margin-top">Balance Statement</p>
                            </a>
                            <a href="#">
                                <span class="in-icon-wrap">
                                    <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-2-icon-2.svg" alt="icon-2" width="38" height="42" data-uk-img>
                                </span>
                                <p class="uk-margin-top">Trading Statement</p>
                            </a>
                            <a href="#">
                                <span class="in-icon-wrap">
                                    <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-2-icon-1.svg" alt="icon-3" width="42" height="42" data-uk-img>
                                </span>
                                <p class="uk-margin-top">Commission</p>
                            </a>
                            <a href="#">
                                <span class="in-icon-wrap">
                                    <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-2-icon-3.svg" alt="icon-4" width="42" height="42" data-uk-img>
                                </span>
                                <p class="uk-margin-top">Margin In-Out</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section uk-section-primary uk-preserve-color in-equity-1">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h4 class="text-white" id="products">Popular products</h4>
                    </div>
                </div>
                <div class="uk-grid-match uk-grid-medium uk-child-width-1-4@m uk-child-width-1-2@s uk-margin-bottom" data-uk-grid>
                    <div>
                        <div class="uk-card uk-card-body uk-card-default uk-border-rounded">
                            <div class="uk-flex uk-flex-middle">
                                <span class="in-product-name red">EQ</span>
                                <h5 class="uk-margin-remove text-white">Stocks</h5>
                            </div>
                            <p class="text-white">Access 25+ currencies across core and emerging markets on 40+ exchanges worldwide.</p>
                            <a href="#" class="uk-button uk-button-text uk-float-right uk-position-bottom-right text-white">Explore<i class="fas fa-arrow-circle-right uk-margin-small-left text-white"></i></a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-body uk-card-default uk-border-rounded">
                            <div class="uk-flex uk-flex-middle">
                                <span class="in-product-name green">LQ</span>
                                <h5 class="uk-margin-remove text-white">Listed Options</h5>
                            </div>
                            <p class="text-white">Access listed options across equities, indices, interest rates, energy, metals and more.</p>
                            <a href="#" class="uk-button uk-button-text uk-float-right uk-position-bottom-right text-white">Explore<i class="fas fa-arrow-circle-right uk-margin-small-left text-white"></i></a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-body uk-card-default uk-border-rounded">
                            <div class="uk-flex uk-flex-middle">
                                <span class="in-product-name blue">FU</span>
                                <h5 class="uk-margin-remove text-white">Futures</h5>
                            </div>
                            <p class="text-white">Access futures covering equity indices, energy, metals, agriculture, rates and more.</p>
                            <a href="#" class="uk-button uk-button-text uk-float-right uk-position-bottom-right text-white">Explore<i class="fas fa-arrow-circle-right uk-margin-small-left text-white"></i></a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-body uk-card-default uk-border-rounded">
                            <div class="uk-flex uk-flex-middle">
                                <span class="in-product-name"><i class="fas fa-ellipsis-h fa-xs"></i></span>
                                <h5 class="uk-margin-remove text-white">More products</h5>
                            </div>
                            <p class="text-white">Explore the full range of cash and leveraged products</p>
                            <a href="#" class="uk-button uk-button-text uk-float-right uk-position-bottom-right text-white">Explore<i class="fas fa-arrow-circle-right uk-margin-small-left text-white"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section in-equity-3 in-offset-top-20">
            <div class="uk-container uk-margin-large-bottom">
                <div class="uk-grid uk-flex uk-flex-middle">
                    <div class="uk-width-expand@m" id="features">
                        <h1 class="uk-margin-small-bottom">Tight spreads and <span class="in-highlight">ultra-fast</span> execution</h1>
                        <h3 class="uk-margin-top uk-text-warning">Best market prices available so your client can receive excellent conditions.</h3>
                        <hr class="uk-margin-medium-top uk-margin-medium-bottom">
                        <ul class="uk-list in-list-check">
                            <li>Negative balance protection</li>
                            <li>Segregated and supervised client funds</li>
                            <li>Instant deposit & fast withdrawal</li>
                        </ul>
                    </div>
                    <div class="uk-width-2xlarge uk-flex uk-flex-right uk-flex-center@s">
                        <div class="uk-card uk-card-body uk-card-default uk-border-rounded in-margin-top-60@s">
                            <div id="tradingview-widget"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        {{-- <!-- section content begin -->
        <div class="uk-section in-equity-4">
            <div class="uk-container uk-margin-top uk-margin-medium-bottom">
                <div class="uk-grid uk-child-width-1-2@m in-testimonial-2" data-uk-grid>
                    <div class="uk-width-1-1@m uk-text-center">
                        <h1>More than <span class="in-highlight">23,000</span> traders joined</h1>
                    </div>
                    <div>
                        <div class="uk-background-contain uk-background-top-left" data-src="{{url('/')}}/equity/img/in-equity-4-blob-1.svg" data-uk-img>
                            <div class="uk-flex uk-flex-middle">
                                <div class="uk-margin-right">
                                    <div class="uk-background-primary uk-border-pill">
                                        <img class="uk-align-center uk-border-pill" src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/blockit/in-team-1.png" alt="client-1" width="100" height="100" data-uk-img>
                                    </div>
                                </div>
                                <div>
                                    <h5 class="uk-margin-remove-bottom">Angela Nannenhorn</h5>
                                    <p class="uk-text-muted uk-margin-remove-top">from United Kingdom</p>
                                </div>
                            </div>
                            <blockquote>
                                <p>Very convenience for trader, spread for gold is relatively low compare to other broker</p>
                            </blockquote>
                        </div>
                    </div>
                    <div>
                        <div class="uk-background-contain uk-background-top-left" data-src="{{url('/')}}/equity/img/in-equity-4-blob-2.svg" data-uk-img>
                            <div class="uk-flex uk-flex-middle">
                                <div class="uk-margin-right">
                                    <div class="uk-background-primary uk-border-pill">
                                        <img class="uk-align-center uk-border-pill" src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/blockit/in-team-8.png" alt="client-2" width="100" height="100" data-uk-img>
                                    </div>
                                </div>
                                <div>
                                    <h5 class="uk-margin-remove-bottom">Wade Palmer</h5>
                                    <p class="uk-text-muted uk-margin-remove-top">from Germany</p>
                                </div>
                            </div>
                            <blockquote>
                                <p>One of the best FX brokers, I have been using! their trading conditions are excellent</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="uk-width-1-1@m uk-text-center">
                        <a href="#" class="uk-button uk-button-text">See more traders stories from all over the world<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end --> --}}
        <!-- section content begin -->
        <div class="uk-section in-equity-5">
            <div class="uk-container uk-margin-remove-bottom">
                <div class="uk-grid uk-child-width-1-3@m uk-child-width-1-2@s" data-uk-grid>
                    <div>
                        <div class="uk-flex uk-flex-left in-award">
                            <div class="uk-margin-small-right">
                                <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-5-award-1.svg" alt="award-1" width="91" height="82" data-uk-img>
                            </div>
                            <div>
                                <h6>Best Execution</h6>
                                <p class="provider">in miliseconds</p>
                                <p class="year"> +/- 25ms</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-flex uk-flex-left in-award">
                            <div class="uk-margin-small-right">
                                <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-5-award-2.svg" alt="award-2" width="91" height="82" data-uk-img>
                            </div>
                            <div>
                                <h6>Best Spread</h6>
                                <p class="provider">in point</p>
                                <p class="year"> +/- 3pts</p>
                            </div>
                        </div>
                    </div>

                        <div class="uk-flex uk-flex-left in-award">
                            <div class="uk-margin-small-right">
                                <img src="{{url('/')}}/equity/img/in-lazy.gif" data-src="{{url('/')}}/equity/img/in-equity-5-award-3.svg" alt="award-3" width="91" height="82" data-uk-img>
                            </div>
                            <div>
                                <h6>Best Commission</h6>
                                <p class="provider">Start from</p>
                                <p class="year"> 3$/lot</p>
                            </div>
                        </div>

                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section uk-section-primary uk-preserve-color in-equity-6 uk-background-contain uk-background-center" data-src="{{url('/')}}/equity/img/in-equity-decor-2.svg" data-uk-img>
            <div class="uk-container uk-margin-small-bottom">
                <div class="uk-grid uk-flex uk-flex-center">
                    <div class="uk-width-2xlarge@m uk-text-center">
                        <h1>Ready to get started?</h1>
                        <p class="uk-text-lead">Join our profitable Introducing Brokerage</p>
                    </div>
                    <div class="uk-width-3-4@m uk-margin-medium-top">
                        <div class="uk-flex uk-flex-center uk-flex-middle button-app">
                            <div>
                                <a href="{{env('IB_REGISTER')}}" class="uk-button uk-button-secondary uk-border-rounded">Open your account<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
    </main>
    <footer>
        <!-- footer content begin -->
        <div class="uk-section">
            <div class="uk-container uk-margin-top">
                <div class="uk-grid">
                    <div class="uk-width-2-3@m">
                        <div class="uk-child-width-1-2@s uk-child-width-1-3@m" data-uk-grid>
                            <div>
                                <h5>Instruments</h5>
                                <ul class="uk-list uk-link-text">
                                    <li><a href="#">Stock</a></li>
                                    <li><a href="#">Indexes</a></li>
                                    <li><a href="#">Currencies</a></li>
                                    <li><a href="#">Metals<span class="uk-label uk-margin-small-left in-label-small">Popular</span></a></li>
                                    <li><a href="#">Oil and gas</a></li>
                                    <li><a href="#">Cryptocurrencies<span class="uk-label uk-margin-small-left in-label-small">Popular</span></a></li>
                                </ul>
                            </div>
                            <div>
                                <h5>Analytics</h5>
                                <ul class="uk-list uk-link-text">
                                    <li><a href="#">World Markets</a></li>
                                    <li><a href="#">Trading Central<span class="uk-label uk-margin-small-left in-label-small">New</span></a></li>
                                    <li><a href="#">Forex charts online</a></li>
                                    <li><a href="#">Market calendar</a></li>
                                </ul>
                            </div>
                            <div class="in-margin-top-60@s">
                                <h5>Education</h5>
                                <ul class="uk-list uk-link-text">
                                    <li><a href="#">Basic course IB</a></li>
                                    <li><a href="#">Introductory webinar</a></li>
                                    <li><a href="#">IB Partnership 101</a></li>
                                    <li><a href="#">About academy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <hr class="uk-margin-large">
            <div class="uk-container">
                <div class="uk-grid uk-flex uk-flex-middle">
                    <div class="uk-width-2-3@m uk-text-small">
                        <ul class="uk-subnav uk-subnav-divider uk-visible@s" data-uk-margin>
                            <li><a href="#">Risk disclosure</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Return policy</a></li>
                            <li><a href="#">Partner Agreement</a></li>
                        </ul>
                        <p class="copyright-text">©2022 Investorich Incorporated. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content end -->
        <!-- totop begin -->
        <div class="uk-visible@m">
            <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
        </div>
        <!-- totop end -->
    </footer>
    <!-- javascript -->
    <script src="{{url('/')}}/equity/js/vendors/uikit.min.js"></script>
    <script src="{{url('/')}}/equity/js/vendors/utilities.min.js"></script>
    <script src="{{url('/')}}/equity/js/vendors/trading-widget.min.js"></script>
    <script src="{{url('/')}}/equity/js/vendors/particles.min.js"></script>
    <script src="{{url('/')}}/equity/js/config-particles.js"></script>
    <script src="{{url('/')}}/equity/js/config-theme.js"></script>
</body>

</html>
